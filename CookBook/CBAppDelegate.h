//
//  CBAppDelegate.h
//  CookBook
//
//  Created by Louise Cavillon on 15/07/2014.
//  Copyright (c) 2014 Louise Cavillon. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "CBRecipe.h"
#import "CBViewControllerRecipeWindow.h"

@interface CBAppDelegate : NSObject <NSApplicationDelegate, NSTextFieldDelegate>
{
    CBRecipe *curr;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSWindow *addRecipeWindow;
@property (retain) IBOutlet NSTextField *name_field;
@property NSMutableArray *recipes;
//@property NSViewController *controller;
//@property NSMutableArray *ingredients;


//- (void) insertRecipeWithName:(NSString*)n withIngredients:(NSMutableArray*)t withSteps:(NSMutableArray*)s;

- (IBAction)insertRecipe:(id)sender;
- (IBAction) cancelFromAddRecipeWindow:(id)sender;



@end
