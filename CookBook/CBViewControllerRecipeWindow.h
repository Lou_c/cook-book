//
//  CBViewControllerRecipeWindow.h
//  CookBook
//
//  Created by Louise Cavillon on 17/07/2014.
//  Copyright (c) 2014 Louise Cavillon. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "CBRecipe.h"

@interface CBViewControllerRecipeWindow : NSViewController<NSTextFieldDelegate>
//<NSTableViewDelegate, NSTableViewDataSource, NSTextFieldDelegate>


@end
