//
//  CBRecipe.m
//  CookBook
//
//  Created by Louise Cavillon on 15/07/2014.
//  Copyright (c) 2014 Louise Cavillon. All rights reserved.
//

#import "CBRecipe.h"

@implementation CBRecipe
@synthesize name;
//@synthesize rating;
//@synthesize ingredients;
//@synthesize steps;
//@synthesize img;

- (id) initRecipeWithName:(NSString*)n andRating:(int)r andIngredients:(NSMutableArray*)t andSteps:(NSMutableArray*)s andImg:(CIImage*)i
{
    self = [super init];
    [n retain];
    [t retain];
    [s retain];
    [i retain];
    [name release];
//    [ingredients release];
//    [steps release];
//    [img release];
    name = n;
//    rating = r;
//    ingredients = t;
//    steps = s;
//    img = i;
    return self;
    
}

- (id) initRecipeWithName:(NSString*)the_name
{
    self = [super init];
    [the_name retain];
    [name release];
    name = the_name;
    return self;
}

@end
