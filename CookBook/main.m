//
//  main.m
//  CookBook
//
//  Created by Louise Cavillon on 15/07/2014.
//  Copyright (c) 2014 Louise Cavillon. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
