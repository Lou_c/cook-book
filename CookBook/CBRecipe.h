//
//  CBRecipe.h
//  CookBook
//
//  Created by Louise Cavillon on 15/07/2014.
//  Copyright (c) 2014 Louise Cavillon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBRecipe : NSObject

@property(nonatomic, retain) NSString *name;
//@property int rating;
//@property NSMutableArray *ingredients;
//@property NSMutableArray *steps;
//@property CIImage *img;
//
//- (id) initRecipeWithName:(NSString*)n andRating:(int)r andIngredients:(NSMutableArray*)t andSteps:(NSMutableArray*)s andImg:(CIImage*)i;

- (id) initRecipeWithName:(NSString*)the_name;
@end
