//
//  CBRecette.m
//  CookBook
//
//  Created by Louise Cavillon on 15/07/2014.
//  Copyright (c) 2014 Louise Cavillon. All rights reserved.
//

#import "CBRecette.h"

@implementation CBRecette

@synthesize name;
@synthesize rating;
@synthesize ingredients;
@synthesize steps;
@synthesize img;

- (id) initRecetteWithName:(NSString*)n andRating:(int)r andIngredients:(NSMutableArray*)t andSteps:(NSMutableArray*)s andImg:(CIImage*)i
{
    self = [super init];
    [n retain];
    [t retain];
    [s retain];
    [i retain];
    [name release];
    [ingredients release];
    [steps release];
    [img release];
    name = n;
    rating = r;
    ingredients = t;
    steps = s;
    img = i;
    return self;
    
}

@end
