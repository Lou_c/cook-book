//
//  CBAppDelegate.m
//  CookBook
//
//  Created by Louise Cavillon on 15/07/2014.
//  Copyright (c) 2014 Louise Cavillon. All rights reserved.
//

#import "CBAppDelegate.h"

@implementation CBAppDelegate

@synthesize window;
@synthesize addRecipeWindow;
@synthesize name_field;
//@synthesize controller;
//@synthesize ingredients;
@synthesize recipes;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    [window makeKeyAndOrderFront:self];
    [addRecipeWindow orderOut:self];
    recipes = [[NSMutableArray alloc] init];
    curr = [[CBRecipe alloc] initRecipeWithName:@""];
    NSLog(@"entered the applicationDidFinishLaunching");
    name_field = [[[NSTextField alloc] init] autorelease];
    [name_field setDelegate:self];
}

- (IBAction) plusButton:(id)sender
{
    [addRecipeWindow makeKeyAndOrderFront:self];
}

//- (IBAction) insertRecipeWithName:(NSString*)the_n withIngredients:(NSMutableArray*)the_t withSteps:(NSMutableArray*)the_s
//{
//    [the_n retain];
//    [the_t retain];
//    [the_s retain];
//    CBRecipe *recipe = [[CBRecipe alloc] initRecipeWithName:the_n andRating:0 andIngredients:the_t andSteps:the_s andImg:nil];
//    [recipes addObject:recipe];
//}

//- (IBAction) insertRecipeWithName:(NSString*)the_name withIngredients:(NSMutableArray*)the_ingr withSteps:(NSMutableArray*)the_steps withRating:(int)i withImg:(CIImage*)the_img
//{
//    [the_name retain];
//    [the_ingr retain];
//    [the_steps retain];
//    [the_img retain];
//    CBRecipe *recipe = [[CBRecipe alloc] initRecipeWithName:the_name andRating:i andIngredients:the_ingr andSteps:the_steps andImg:the_img];
//    [recipes addObject:recipe];
//    
//}

- (IBAction)insertRecipe:(id)sender
{
    NSLog(@"Entered the insertRecipe function\n");
    NSLog(@"The current name of curr is : %@", curr.name);
    [recipes addObject:curr];
    for (CBRecipe* recipe_tmp in recipes)
    {
        NSLog(@"Current string in list : %@ \n", recipe_tmp.name);
    }
}


- (void) controlTextDidEndEditing:(NSNotification *)notif
{
    NSLog(@"Entered the controlTextDidEndEditing");
    NSTextField *tmp = [notif object];
    NSLog(@"The textfield changed, current value : %@", tmp.stringValue);
    [curr release];
    curr = [[CBRecipe alloc] initRecipeWithName:@""];
    curr.name = tmp.stringValue;
}


- (IBAction) cancelFromAddRecipeWindow:(id)sender
{
    [window makeKeyAndOrderFront:self];
    [addRecipeWindow orderOut:self];
    NSLog(@"In the cancel !");
}

@end
