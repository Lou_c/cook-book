//
//  CBIngredient.h
//  CookBook
//
//  Created by Louise Cavillon on 15/07/2014.
//  Copyright (c) 2014 Louise Cavillon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBIngredient : NSObject

@property int quantity;
@property NSString *name;
@property NSString *container;

- (id) initIngredientWithQuantity:(int)q andName:(NSString*)n andContainer:(NSString*)c;

@end
