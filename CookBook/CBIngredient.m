//
//  CBIngredient.m
//  CookBook
//
//  Created by Louise Cavillon on 15/07/2014.
//  Copyright (c) 2014 Louise Cavillon. All rights reserved.
//

#import "CBIngredient.h"

@implementation CBIngredient

@synthesize quantity;
@synthesize name;
@synthesize container;


- (id) initIngredientWithQuantity:(int)q andName:(NSString*)n andContainer:(NSString*)c
{
    self = [super init];
    quantity = q;
    [n retain];
    [c retain];
    [name release];
    [container release];
    name = n;
    container = c;
    return self;
}



@end
